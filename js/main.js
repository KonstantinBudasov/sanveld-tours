function sideBarHeight() {
    var sideBarHeight = $(".l-main-wrapper").innerHeight();
    $(".l-region-main").css("min-height" , sideBarHeight);
    $(".l-region-aside").css("min-height" , sideBarHeight);
}

function equalPostColumnsHeight() {
    var postTextHeight = $(".post-text").innerHeight();
    var postImagesHeight = $(".post-images").innerHeight();

    if (postTextHeight > postImagesHeight) {
        $(".post-images").innerHeight(postTextHeight);
    } else {
        $(".post-text").innerHeight(postImagesHeight);
    }
}

$(document).ready(function () {

    // Sidebar viewport height
    sideBarHeight();

    // Equal post columns height
    equalPostColumnsHeight();

    // Slick Slider init
    $('.slider').slick({
		dots: true,
        arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		adaptiveHeight: true
    });

    // Sidebar menu accordion
    (function($) {

        var allPanels = $('.aside-nav .submenu').hide();

        $('.aside-nav-item-ln').on("click touchstart", function(){
            allPanels.slideUp();
            if($(this).next().is(":visible")) {
                $(this).next().slideUp;
            } else {

                $(this).next().slideDown();
                return false;
            }
        });

        $(".submenu-item-ln").on("hover", function(){
            $(this).parent().addClass("submenu-item-active");
        });

    })(jQuery);

    (function($) {
        $(".posts-list li:odd").addClass("odd");
    })(jQuery);

    (function($) {
        $(".teasers-list li:last-child").addClass("last-child");
    })(jQuery);

    (function($) {
        $(".registration-steps-manual li:last-child").addClass("last-child");
    })(jQuery);
});
